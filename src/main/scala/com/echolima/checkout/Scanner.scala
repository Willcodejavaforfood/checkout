package com.echolima.checkout

object Scanner extends Pricing {
  def checkout(items: List[String]): String = items match {
    case Nil => formatPrice(0)
    case h::t => checkout_impl(items)
  }

  def countItems(items: List[String]): Map[String, Int] = {
    items
      .groupBy(identity)
      .mapValues(_.size)
  }

  def costItems(itemCounts: Map[String, Int]): Int = {
    itemCounts
      .map({case (item, count) => (item, itemDiscounts(item.toUpperCase)(count))})
      .map({case (item, count) => defaultPrices(item.toUpperCase) * count})
      .foldLeft(0)(_ + _)
  }

  private def checkout_impl = countItems _ andThen costItems _ andThen formatPrice _
}
