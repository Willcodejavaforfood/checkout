package com.echolima.checkout

trait Pricing {
  val defaultPrices = Map(
    "APPLE" -> 60,
    "ORANGE" -> 25
  )

  val itemDiscounts = Map[String, (Int) => Int](
    "APPLE" -> appleDiscount,
    "ORANGE" -> orangeDiscount
  )

  def formatPrice(price: Int): String = f"£${price / 100.0}%2.2f"

  def appleDiscount(count: Int): Int = Math.ceil(count * 0.5).toInt

  def orangeDiscount(count: Int): Int = Math.ceil(count * 0.66).toInt

}
