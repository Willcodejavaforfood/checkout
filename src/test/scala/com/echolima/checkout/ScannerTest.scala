package com.echolima.checkout

import org.scalatest._

class ScannerSpec extends FlatSpec with Matchers {

  "The Scanner" should "correctly cost an empty cart" in {
    Scanner.checkout(List()) should be ("£0.00")
  }

  "The Scanner" should "correctly sum up the cost of items in a cart upon checkout" in {
    Scanner.checkout(List("Apple", "Orange", "Orange", "Orange")) should be ("£1.10")
    Scanner.checkout(List("Apple", "Apple", "Orange", "Apple")) should be ("£1.45")
    Scanner.checkout(List("Apple")) should be ("£0.60")
    Scanner.checkout(List("Orange")) should be ("£0.25")
    Scanner.checkout(List("Apple", "Orange", "Orange")) should be ("£1.10")
  }

  "The Scanner" should "count items by type" in {
    Scanner.countItems(List("Orange", "Apple", "Apple")) should be (Map("Apple" -> 2, "Orange" -> 1))
  }

  "The Scanner" should "cost items as per Pricing config" in {
    Scanner.costItems(Map("Apple" -> 2, "Orange" -> 1)) should be (85)
  }
}
